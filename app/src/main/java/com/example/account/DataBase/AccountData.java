package com.example.account.DataBase;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class AccountData {
    @PrimaryKey(autoGenerate = true)
    private int id;//id
    @ColumnInfo(name="name")
    private String name;
    @ColumnInfo(name="imageid")
    private int imageid;
    @ColumnInfo(name="bz")
    private String bz;
    @ColumnInfo(name="time")
    private String time;
    @ColumnInfo(name="year")
    private int year;
    @ColumnInfo(name="month")
    private int month;
    @ColumnInfo(name="day")
    private int day;
    @ColumnInfo(name="type")
    private int type;//收入1 支出0
    @ColumnInfo(name="money")
    private float money;
    @Ignore
    public AccountData(){

    }

    public AccountData(String name, int imageid, String bz, String time, int year, int month, int day, int type, float money) {
        this.name = name;
        this.imageid = imageid;
        this.bz = bz;
        this.time = time;
        this.year = year;
        this.month = month;
        this.day = day;
        this.type = type;
        this.money = money;
    }

    public float getMoney() {
        return money;
    }

    public void setMoney(float money) {
        this.money = money;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImageid() {
        return imageid;
    }

    public void setImageid(int imageid) {
        this.imageid = imageid;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "AccountData{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", imageid=" + imageid +
                ", bz='" + bz + '\'' +
                ", time='" + time + '\'' +
                ", year=" + year +
                ", month=" + month +
                ", day=" + day +
                ", type=" + type +
                ", money=" + money +
                '}';
    }
}
