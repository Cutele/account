package com.example.account.DataBase;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class TypeData {
    //主键id，名字，选中的图标编号，未选中的图标编号，收入/支出
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name="name")
    private String name;
    @ColumnInfo(name="yi_id")
    private int yi_id;
    @ColumnInfo(name="wei_id")
    private int wei_id;
    @ColumnInfo(name="kind")
    private int kind;//1 支出 -1收入
    @Ignore
    public TypeData(){

    }
    public TypeData(String name, int yi_id, int wei_id, int kind) {
        this.name = name;
        this.yi_id = yi_id;
        this.wei_id = wei_id;
        this.kind = kind;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYi_id() {
        return yi_id;
    }

    public void setYi_id(int yi_id) {
        this.yi_id = yi_id;
    }

    public int getWei_id() {
        return wei_id;
    }

    public void setWei_id(int wei_id) {
        this.wei_id = wei_id;
    }

    public int getKind() {
        return kind;
    }

    public void setKind(int kind) {
        this.kind = kind;
    }

    @Override
    public String toString() {
        return "TypeData{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", yi_id=" + yi_id +
                ", wei_id=" + wei_id +
                ", kind=" + kind +
                '}';
    }
}
