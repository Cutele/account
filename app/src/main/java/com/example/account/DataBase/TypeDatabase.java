package com.example.account.DataBase;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {TypeData.class},version = 1,exportSchema = false)
public abstract class TypeDatabase extends RoomDatabase {
    private static TypeDatabase INSTANCE;
    static public TypeDatabase getDatabase(Context context){
        if(INSTANCE==null) {
            INSTANCE= Room.databaseBuilder(context,TypeDatabase.class,"type_database")
                    .allowMainThreadQueries().build();
            //数据库名 type_database
            //表名 TypeData
        }
        return INSTANCE;
    }
    public abstract TypeDataDao getDataDao();
}
