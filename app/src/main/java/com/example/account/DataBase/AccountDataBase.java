package com.example.account.DataBase;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {AccountData.class},version = 1,exportSchema = false)
public abstract class AccountDataBase extends RoomDatabase {
    private static AccountDataBase INSTANCE;
    static public AccountDataBase getDatabase(Context context){
        if(INSTANCE==null) {
            INSTANCE= Room.databaseBuilder(context,AccountDataBase.class,"account_database")
                    .allowMainThreadQueries().build();
            //数据库名 note_database
            //表名 Data
        }
        return INSTANCE;
    }
    public abstract AccountDataDao getDataDao();
}
