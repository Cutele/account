package com.example.account.DataBase;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface AccountDataDao {
    //收入1 支出0
    @Insert
    void insertAccountData(AccountData data);
    @Query("delete from AccountData")
    void deleteAll();
    @Query("delete from AccountData where id=:pos")
    int DeleteById(int pos);
    @Query("select * from AccountData where id=:pos")
    AccountData FindById(int pos);
    @Query("select * from AccountData")
    List<AccountData> findAll();
    @Query("select money from AccountData where type=:pos")
    float qaskMoneySum(int pos);//询问某一类型的总累计和
    @Query("select money from AccountData where type=:pos and day=:d and month=:m and year=:y")
    float qaskMoney(int pos,int y,int m,int d);//询问某一天某一类型的总累计和
    @Query("select * from AccountData where month=:m and year=:y")
    List<AccountData> qaskMoneyByMonth(int m,int y);
    @Query("select money from AccountData where type=:pos and month=:m and year=:y")
    float query_Money_by_month_type(int pos,int m,int y);
    @Query("select * from AccountData where day=:d and month=:m and year=:y")
    List<AccountData> qask_by_ymd(int d,int m,int y);
    @Update
    int Update(AccountData accountData);
}
