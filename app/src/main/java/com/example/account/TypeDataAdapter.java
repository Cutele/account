package com.example.account;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.account.DataBase.TypeData;

import java.util.List;

public class TypeDataAdapter extends BaseAdapter {
    List<TypeData> list;
    Context context;
    int selectPos = -1;  //选中位置
    public TypeDataAdapter(Context context,List<TypeData> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view= LayoutInflater.from(context).inflate(R.layout.item_frag_gv,viewGroup,false);
        ImageView iv=view.findViewById(R.id.item_frag_iv);
        TextView tv=view.findViewById(R.id.item_frag_tv);
        TypeData now=list.get(i);
        tv.setText(now.getName());
        if(selectPos==i){
            iv.setImageResource(now.getYi_id());
        }
        else{
            iv.setImageResource(now.getWei_id());
        }
        return view;
    }
}
